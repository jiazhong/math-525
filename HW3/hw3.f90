! This program is to solve the linear system AX = C by Gauss Jordan Elimination
! A is m by m
! X is m by k
! C is m by k


PROGRAM hw3_2015

use omp_lib

implicit none

integer, parameter  :: m = 1024, k = m, n = m+k
integer             :: i,j
double precision    :: tmp, A(m,m), X(m,k), XE(m,k), B(m,n), C(m,k)
double precision    :: RESIDUAL(m,k), t0, t1, time, gflops

integer             :: p
double precision    :: maxB
double precision    :: RESIDUAL_max, ERROR_max

! initialize arrays
  call random_number(A)
  B(1:m,1:m) = A

  call random_number(XE)  ! XE is the "exact" solution
  C = matmul(A,XE)
  B(1:m, (m+1):(m+k)) = C

! print
!  write(*, *)
!  write(*, *) 'B Matrix:'
!  do i = 1, m
!     do j = 1, n
!        write(*, '(3f8.3)', advance='no'), B(i,j)
!     enddo
!     write(*, *)
!  enddo

  t0 = omp_get_wtime()

  do i = 1, m
     
     ! find out the row that has element of largest absolute value

     maxB = -100

     do j = i, m
        tmp = abs(B(j,i))
        if (tmp > maxB) THEN
           p = j
           maxB = tmp
        endif
     enddo

     ! interchange this row with row when abs(B(i,i)) is 
     ! not the largest absolute value in column i
     
     if (p /= i) THEN
        do j = 1, n
           tmp = B(i,j)
           B(i,j) = B(p,j)
           B(p,j) = tmp
        enddo
     endif

     ! normalize row i with row(i,i)

     B(i,1:n) = B(i,1:n)/B(i,i)

     ! eliminate above and below diagonal to zero
   
     do j = 1, m
        if (j == i) THEN
        ! do nothing here
        elseif (j /= i ) THEN
           B(j,1:n) = B(j,1:n) - B(j,i)*B(i,1:n)
        endif
     enddo
  
  enddo
  
  t1 = omp_get_wtime()
  time = t1 - t0 ! time in seconds
  gflops = (((dble(k)*dble(n)**3)/3)/time)/(10**9)

  X(1:m, 1:k) = B(1:m, (m+1):(m+k))

  RESIDUAL =matmul(A,X) - C
  RESIDUAL_max = maxval(abs(RESIDUAL))
  ERROR_max = maxval(abs(XE - X))
! print checking Matrix
!  write(*, *)
!  write(*, *) 'Checking Matrix:'
!  do i = 1, m
!     do j = 1, n
!        write(*, '(3f8.3)', advance='no'), B(i,j)
!     enddo
!     write(*, *)
!  enddo

! print X
!  write(*, *)
!  write(*, *) 'X:'
!  do i = 1, m 
!     do j = 1, k
!        write(*, '(3f8.3)', advance='no'), X(i,j)
!     enddo
!     write(*, *)
!  enddo

! print XE
!  write(*, *)
!  write(*, *) 'XE:'
!  do i = 1, m
!     do j = 1, k
!        write(*, '(3f8.3)', advance='no'), XE(i,j)
!     enddo
!     write(*, *)
!  enddo

! print
  write(*, *)
  print*, 'Glops = ', gflops
  print*, 'Time = ', time
  print*, 'm = ', m
  print*, 'k = ', k
  print*, 'RESIDUAL = ', RESIDUAL_max
  print*, 'maximum ERROR = ', ERROR_max  
END PROGRAM hw3_2015

! OUTPUT
! Glops =    155.104477866765
! Time =    18.9035871028900
! m =         1024
! k =         1024
! RESIDUAL =   1.364242052659392E-012
! maximum ERROR =   4.462652469783279E-012

! Glops =    155.423389556311
! Time =    18.8647990226746
! m =         1024
! k =         1024
! RESIDUAL =   1.364242052659392E-012
! maximum ERROR =   4.462652469783279E-012





