PROGRAM main

  use mpi
  use stat
  implicit none
  integer, parameter  :: n = 1024, ntrial = 32
  double precision    :: flush(3*1024*1024)
  integer, parameter  :: dp = mpi_double_precision, comm = mpi_comm_world
  double precision    :: t0, t1, time(0:ntrial), max_time(0:ntrial), avg, gflops
  double precision    :: x, A(n,n), B(n,n), C(n,n), D(n,n)
  double precision    :: tmp1, tmp2, tmp3, tmp4
  integer             :: i, j, k, ierror, bk, ii, jj, kk
  integer             :: p, rank, status(mpi_status_size), ktrial

  call mpi_init(ierror)
  call mpi_comm_size(comm, p, ierror)
  call mpi_comm_rank(comm, rank, ierror)

  call random_number(A)
  call random_number(B)

  bk = 512

! serial code
  do ktrial = 0, ntrial
     C = 0.d0
     call random_number(flush)
     call mpi_barrier(comm, ierror)
     t0 = mpi_wtime()
     if (rank == 0) then 
         do i = 1, n 
            do j = 1, n 
               do k = 1, n
                  C(i,j) = C(i,j) + A(i,k)*B(k,j)
               enddo
            enddo
         enddo

         !write(*, *)
         !write(*, *) 'Matrix'
         !do i = 1,n
         !   do j = 1,n
         !      write(*, '(3f8.3)', advance='no'), C(i,j)
         !   enddo
         !   write(*, *)
         !enddo
     endif
     t1 = mpi_wtime()
     call mpi_barrier(comm, ierror)
     time(ktrial) = t1-t0
  enddo
  call mpi_reduce(time, max_time, 1+ntrial, dp, mpi_max, 0, comm, ierror)
  if (rank == 0) then
     avg = timestat(ntrial, max_time)
     gflops = ((2.d0*dble(n)**3)/avg)/(10**9)
     print*, 'n=', n, 'ktrial=', ktrial, 'gflops=', gflops
     print*, 'prevent dead code elimination: C(3,7)= ', C(3,11)
  endif

! optimized code
! use Stride
! loop unrolling
! transpose inner loop matrix A
! break down loop into blocks

  do ktrial = 0, ntrial
     C = 0.d0
     call random_number(flush)
     call mpi_barrier(comm, ierror)
     t0 = mpi_wtime()
     if (rank == 0) then
         do kk = 1, n, bk
            do ii = 1, n, bk 
               do i = ii, min(ii+bk-1,n)
                  do k = kk, min(kk+bk-1,n)
                     D(k-kk+1,i-ii+1) = A(i,k)
                  enddo
               enddo
               do jj = 1, n, bk
                  do j = jj, min(jj+bk-1,n), 2
                     do i = ii, min(ii+bk-1,n), 2
                        tmp1 = 0.d0
                        tmp2 = 0.d0
                        tmp3 = 0.d0
                        tmp4 = 0.d0
                        do k = kk, min(kk+bk-1,n)
                           tmp1 = tmp1 + D(k-kk+1,i-ii+1)*B(k,j)
                           tmp2 = tmp2 + D(k-kk+1,i-ii+2)*B(k,j)
                           tmp3 = tmp3 + D(k-kk+1,i-ii+1)*B(k,j+1)
                           tmp4 = tmp4 + D(k-kk+1,i-ii+2)*B(k,j+1)
                        enddo
                        C(i,j) = C(i,j) + tmp1
                        C(i+1,j) = C(i+1,j) + tmp2
                        C(i,j+1) = C(i,j+1) + tmp3
                        C(i+1,j+1) = C(i+1,j+1) + tmp4
                     enddo
                  enddo
               enddo
            enddo
         enddo
         !write(*, *) 
         !write(*, *) 'Matrix'
         !do i = 1, n
         !   do j = 1, n
         !      write(*, '(3f8.3)', advance = 'no'), C(i,j)
         !   enddo
         !   write(*, *)
         !enddo
     endif
     t1 = mpi_wtime()
     call mpi_barrier(comm, ierror)
     time(ktrial) = t1-t0
  enddo
  call mpi_reduce(time, max_time, 1+ntrial, dp, mpi_max, 0, comm, ierror)
  if (rank == 0) then
     avg = timestat(ntrial, max_time)
     gflops = ((2.d0*dble(n)**3)/avg)/(10**9)
     print*, 'n=', n, 'ktrial=', ktrial, 'gflops=', gflops
     print*, 'prevent dead code elimination: C(3,7) =', C(3,11)
  endif

     call mpi_finalize(ierror)  
END PROGRAM main

! OUTPUT

! Non-Optimization
! the average of t(1:n) is  0.447453401982784
! n=        1024 ktrial=          33 gflops=   4.79934589497796
! prevent dead code elimination: C(3,7)=    254.803517933714

! Optimization
! the average of t(1:n) is  0.228068679571152
! n=        1024 ktrial=          33 gflops=   9.41595159860624
! prevent dead code elimination: C(3,7) =   254.803517933714

