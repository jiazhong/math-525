PROGRAM MPI_HW2

  use mpi
  use stat
  implicit none
  integer, parameter    :: n = 1024, ntrial = 128
  integer, parameter    :: dp = mpi_double_precision, comm = mpi_comm_world
  double precision      :: t0, t1, time(0:ntrial), avg, flush(3*1024*1024)
  double precision      :: A(n), x = 0, s = 0
  integer               :: i, ierror, ktrial
  integer               :: p, rank, status(mpi_status_size)


  call mpi_init(ierror) ! must be called prior to calling other mpi routines
  call mpi_comm_size(comm, p, ierror) ! p = the total number of mpi processes
  call mpi_comm_rank(comm, rank, ierror) ! rank = rank of executing mpi process

  do ktrial = 0, ntrial
     flush = flush + 0.001d0

     if (rank==0) then
        call random_number(A)

        t0 = mpi_wtime()

        do i = 1, p-1
           call mpi_send(A(1), n, dp, i, 0, comm, ierror)
           call mpi_recv(x, 1, dp , i, 0, comm, status, ierror)
           if (x == 1) then
              print*, 'message recieved from processor ', i
              s = s+x
           else
              print*, 'message terminated'
           endif
        enddo

        if (s==p-1) then
           print*, 'All messages have been received'
           s = 0
        endif

        t1 = mpi_wtime()
        time(ktrial) = (t1-t0)*1.d6 ! unit: microseconds

     else
        call mpi_recv(A(1), n, dp, 0, 0, comm, status, ierror)
        x = 1
        call mpi_send(x, 1, dp, 0, 0, comm, ierror)
        x = 0

     endif
  enddo

  if (rank==0) then
     avg = timestat(ntrial,time)
  endif

  call mpi_finalize(ierror)

END PROGRAM MPI_HW2

! OUTPUT
! Times in micro seconds
! Number of Timings = 128                                                      
! t(0) =   0.000000000000000E+000
! the maximum of t(1:n) is   15342.9508209229
! the minimum of t(1:n) is   10178.0891418457
! the average of t(1:n) is   13036.6161465645
! the standard deviation of t(1:n) is   862.458902548611
! the median of t(1:n) is   13236.9995117188

