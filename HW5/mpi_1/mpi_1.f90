PROGRAM MPI_HW1

  use mpi
  implicit none
  integer, parameter     :: n = 2
  integer, parameter     :: dp = mpi_double_precision, comm=mpi_comm_world
  double precision       :: t0, t1, time  ! need when using mpi_wtime
  double precision       :: A(n), B(n), A_print(n), B_print(n)
  integer                :: i, ierror
  integer                :: p, rank, status(mpi_status_size)

  call mpi_init(ierror)  ! must be called prior to calling other mpi routines
  call mpi_comm_size(comm, p, ierror)  ! p = total number of mpi processes
  call mpi_comm_rank(comm, rank, ierror) ! rank = rank of executing mpi process

  ! Initialize the array A on each MPI processes
  call random_number(A)

  A = A + dble(rank)

  if (rank==0) then
     print*, 'Number of Processor is', p
     print*
     print*, 'Processor 0'

     call mpi_send(A(1), n, dp, 1, 0, comm, ierror)

     print*, 'A=', A
     print*

     ! Receive data from other processors for printing in order
     do i =1, p-1
        call mpi_recv(A_print(1), n, dp, i, 0, comm, status, ierror)
        call mpi_recv(B_print(1), n, dp, i, 0, comm, status, ierror)
        print*, 'Processor ', i
        print*, 'A=', A_print
        print*, 'B=', B_print
        print*
     enddo

  elseif (rank==p-1) then
     call mpi_recv(B(1), n, dp, rank-1, 0, comm, status, ierror)

     A(1:n) = A(1:n) + B(1:n)

     ! sent data to processor 0 for printing in order
     call mpi_send(A(1), n, dp, 0, 0, comm, ierror)
     call mpi_send(B(1), n, dp, 0, 0, comm, ierror)

  else
     call mpi_recv(B(1), n, dp, rank-1, 0, comm, status, ierror)

     A(1:n) = A(1:n) + B(1:n)

     call mpi_send(A(1), n, dp, rank+1, 0, comm, ierror)

     ! sent data to processor 0 for printing in order
     call mpi_send(A(1), n, dp, 0, 0, comm, ierror)
     call mpi_send(B(1), n, dp, 0, 0, comm, ierror)

  endif

  call mpi_finalize(ierror)
END PROGRAM MPI_HW1

! OUTPUT
! Number of Processor is           4
 
! Processor 0
! A=  3.920868194323862E-007  2.548044275764261E-002
 
! Processor            1
! A=   1.00000078417364        1.05096088551528     
! B=  3.920868194323862E-007  2.548044275764261E-002
 
! Processor            2
! A=   3.00000117626046        3.07644132827293     
! B=   1.00000078417364        1.05096088551528     
 
! Processor            3
! A=   6.00000156834728        6.10192177103057     
! B=   3.00000117626046        3.07644132827293


! Number of Processor is           8
 
! Processor 0
! A=  3.920868194323862E-007  2.548044275764261E-002
 
! Processor            1
! A=   1.00000078417364        1.05096088551528     
! B=  3.920868194323862E-007  2.548044275764261E-002
 
! Processor            2
! A=   3.00000117626046        3.07644132827293     
! B=   1.00000078417364        1.05096088551528     
 
! Processor            3
! A=   6.00000156834728        6.10192177103057     
! B=   3.00000117626046        3.07644132827293     
 
! Processor            4
! A=   10.0000019604341        10.1274022137882     
! B=   6.00000156834728        6.10192177103057     
 
! Processor            5
! A=   15.0000023525209        15.1528826565459     
! B=   10.0000019604341        10.1274022137882     
 
! Processor            6
! A=   21.0000027446077        21.1783630993035     
! B=   15.0000023525209        15.1528826565459     
 
! Processor            7
! A=   28.0000031366946        28.2038435420611     
! B=   21.0000027446077        21.1783630993035 
