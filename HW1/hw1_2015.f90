MODULE MATRIX
CONTAINS
  SUBROUTINE MATVEC(m, n, y, A, x)
  implicit none
  integer, intent(in)             :: m, n
  double precision, intent(in)    :: A(m,n), x(n)
  double precision, intent(inout) :: y(m)
  integer                         :: i, j

! Computer y = y + Ax

  do i = 1,m
     do j = 1,n
        y(i) =  y(i) + A(i,j)*x(j)
     enddo
  enddo

  END SUBROUTINE MATVEC

  FUNCTION FMATVEC(m, n, y, A, x)
  implicit none
  integer, intent(in)             :: m, n
  double precision, intent(in)    :: A(m,n), x(n)
  double precision, intent(inout) :: y(m)
  double precision                :: fmatvec(m)
  integer                         :: i, j

! Computer y = y + Ax where fmatvec = y

  do i = 1,m
     do j = 1,n
        y(i) = y(i) + A(i,j)*x(j)
     enddo
  enddo

  fmatvec = y

  END FUNCTION FMATVEC

END MODULE MATRIX

PROGRAM hw1_2015
use matrix
implicit none
integer, parameter  :: m = 5, n = 7, k = 11
integer             :: i, j
double precision    :: A0(m,k), A(m,k), B0(k,n), B(k,n), C0(m,n), C(m,n)
double precision    :: error(m,n), x(k), y(m)

! Initialize A0, B0 and C0
  call random_number(C0)
  call random_number(A0)
  call random_number(B0)

! The calculation using the subroutine matvec and initial values of A,B and C
A = A0
B = B0
C = C0 ! use the initial value of C
do j = 1,n
   y(1:m) = C(1:m,j)
   x(1:k) = B(1:k,j)
   call matvec(m, k, y, A, x)
   C(1:m, j) = y(1:m)
enddo

! Compare answer with C0 + matmul(A,B)
error = C - (C0 + matmul(A,B))
print*, 'max abs error using matvec=', maxval(abs(error))

! The calulation using the function fmatvec and the initial values of A, B, C
A = A0
B = B0
C = C0
do j = 1,n
   y(1:m) = C(1:m,j)
   x(1:k) = B(1:k,j)
   C(1:m,j) = fmatvec(m, k, y, A, x)
enddo

! Compare answer using matmul intrinsic
error = C - (C0 + matmul(A,B))
print*, 'max abs error using fmatvec=', maxval(abs(error))

END PROGRAM hw1_2015


! OUTPUT
! max abs error using matvec=  1.332267629550188E-015
! max abs error using fmatvec=  1.332267629550188E-015

