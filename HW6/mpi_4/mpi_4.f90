PROGRAM MPI_HW4

  use mpi
  use stat
  implicit none
  integer, parameter :: n = 64, ntrial = 128
  double precision   :: flush(3*1024*1024) 
  integer, parameter :: dp = mpi_double_precision, comm = mpi_comm_world
  double precision   :: t0, t1, time(0:ntrial), timelocal(0:ntrial), avg
  double precision   :: x, A(n,n), B(n,n), C(n,n), D(n,n)
  integer            :: i, j, ierror, left, right
  integer            :: p, rank, status(mpi_status_size), ktrial
  
  call mpi_init(ierror)
  call mpi_comm_size(comm, p, ierror)
  call mpi_comm_rank(comm, rank, ierror)

  flush = flush + 0.001d0

  call random_number(A)
  A = A + dble(rank)
 
  if (rank==0) then
     left = p-1
  else 
     left = rank-1
  endif

  if (rank==p-1) then
     right = 0
  else
     right = rank+1
  endif

  do ktrial = 0, ntrial
     
     call mpi_barrier(comm, ierror)
     
     t0 = mpi_wtime()
     
     call mpi_send(A(1,1), n*n, dp, right, 0, comm, ierror)
     call mpi_recv(B(1,1), n*n, dp, left,  0, comm, status, ierror)
     C = matmul(A,B)
     call mpi_send(C(1,1), n*n, dp, left,  0, comm, ierror)
     call mpi_recv(D(1,1), n*n, dp, right, 0, comm, status, ierror)

     t1 = mpi_wtime()
     timelocal(ktrial) = (t1-t0)
  enddo

  call mpi_reduce(timelocal(0), time(0), ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
     print*,''
     print*, 'All taks have been completed'
     print*, ''
     print*, 'Time in seconds' 
     print*, 'Number of processor', p
     print*, 'Number of timings = ', ntrial
     avg = timestat(ntrial,time)
     print*, ''
  endif
 
  call mpi_finalize(ierror)

END PROGRAM MPI_HW4

! OUTPUT
! All taks have been completed

! Time in seconds
! Number of processor          16
! Number of timings =          128
! t(0) =   5.640983581542969E-004
! the maximum of t(1:n) is  1.268386840820312E-004
! the minimum of t(1:n) is  1.058578491210938E-004
! the average of t(1:n) is  1.080594956874847E-004
! the standard deviation of t(1:n) is  3.270997929551431E-006
! the median of t(1:n) is  1.070499420166016E-004


! All taks have been completed
!
! Time in seconds
! Number of processor          32
! Number of timings =          128
! t(0) =   6.089210510253906E-004
! the maximum of t(1:n) is  1.997947692871094E-004
! the minimum of t(1:n) is  1.301765441894531E-004
! the average of t(1:n) is  1.405235379934311E-004
! the standard deviation of t(1:n) is  8.104771016361103E-006
! the median of t(1:n) is  1.385211944580078E-004


! All taks have been completed

! Time in seconds
! Number of processor          64
! Number of timings =          128
! t(0) =   5.149841308593750E-004
! the maximum of t(1:n) is  2.119541168212891E-004
! the minimum of t(1:n) is  1.540184020996094E-004
! the average of t(1:n) is  1.727323979139328E-004
! the standard deviation of t(1:n) is  1.003900402499801E-005
! the median of t(1:n) is  1.730918884277344E-004

