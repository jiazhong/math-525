PROGRAM MPI_HW3

  use mpi
  use stat
  implicit none
  integer, parameter :: n = 512, ntrial = 128, nflush = 3*1024*1024
  integer, parameter :: dp = mpi_double_precision, comm = mpi_comm_world
  double precision   :: t0, t1, time(0:ntrial), timelocal(0:ntrial), avg
  double precision   :: A(n,n), B(n,n), C(n,n), flush(nflush)
  integer            :: ierror, ktrial
  integer            :: p, rank, status(mpi_status_size)

  call mpi_init(ierror)
  call mpi_comm_size(comm, p, ierror)
  call mpi_comm_rank(comm, rank, ierror)

  flush = flush + 0.001d0

  call random_number(A)
  call random_number(B)

  do ktrial = 0, ntrial  
     
     call MPI_barrier(comm, ierror)

     t0 = mpi_wtime()

     C = matmul(A,B)

     t1 = mpi_wtime()
 
     timelocal(ktrial) = t1-t0
 
  enddo    

  call MPI_reduce(timelocal(0), time(0), ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then 
     print*, 'Times in seconds'
     print*, 'number of timings = 128'
     avg = timestat(ntrial,time)
  endif

  call mpi_finalize(ierror)

END PROGRAM MPI_HW3

! OUTPUT
! Times in seconds
! number of timings = 128
! t(0) =   7.566595077514648E-002
! the maximum of t(1:n) is  7.586717605590820E-002
! the minimum of t(1:n) is  7.520985603332520E-002
! the average of t(1:n) is  7.543297484517097E-002
! the standard deviation of t(1:n) is  1.024637785066955E-004
! the median of t(1:n) is  7.541954517364502E-002
