PROGRAM MPI_HW8

  use mpi
  use stat
  implicit none
  integer, parameter :: ntrial = 512
  integer            :: i, ktrial
  double precision   :: flush(3*1024*1024) ! to flush an 8*3 MB cache
  integer, parameter :: dp = mpi_double_precision, comm = mpi_comm_world
  integer            :: ierror, p, rank, status(mpi_status_size)
  double precision   :: t, t0, t1, time(0:ntrial), max_time(0:ntrial)
  double precision   :: avg

  call mpi_init(ierror)
  call mpi_comm_size(comm, p, ierror)
  call mpi_comm_rank(comm, rank, ierror)

  if (rank==0) then
    print*, 'Times in microseconds, p=', p
  endif

  call mpi_barrier(comm, ierror)

  do ktrial = 0, ntrial
     call random_number(flush) ! flush caches
     call mpi_barrier(comm, ierror)
     t0 = mpi_wtime()
     call mpi_barrier(comm, ierror)
     t1 = mpi_wtime()
     time(ktrial) = (t1-t0)*1.d6 ! time in microseconds
  enddo

  call mpi_reduce(time, max_time, ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
     print*, 'time to call mpi_barrier is'
     avg = timestat(ntrial, max_time)
  endif

! 1. time the send-recv barrier

  call mpi_barrier(comm, ierror)
  
  do ktrial = 0, ntrial
     call random_number(flush) ! flush caches
     call mpi_barrier(comm, ierror)
     t0 = mpi_wtime()
     call barrier1(p, rank, comm)
     t1 = mpi_wtime()
     call mpi_barrier(comm, ierror)
     time(ktrial) = (t1-t0)*1.d6 ! time in microseconds
  enddo

  call mpi_reduce(time, max_time, ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
     print*, 'time to call mpi_send/mpi_recv barrier is'
     avg = timestat(ntrial, max_time)
  endif

! 2. time the alltoall barrier

  call mpi_barrier(comm, ierror)

  do ktrial = 0, ntrial
     call random_number(flush) ! flush caches
     call mpi_barrier(comm, ierror)
     t0 = mpi_wtime()
     call barrier2(p, rank, comm)
     t1 = mpi_wtime()
     call mpi_barrier(comm,ierror)
     time(ktrial) = (t1-t0)*1.d6 ! time in microseconds
  enddo

  call mpi_reduce(time, max_time, ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
     print*, 'time to call alltoall barrier is'
     avg = timestat(ntrial, max_time)
  endif

! 3. time the bcast barrier

  call mpi_barrier(comm, ierror)

  do ktrial = 0, ntrial
     call random_number(flush) ! flush caches
     call mpi_barrier(comm, ierror)
     t0 = mpi_wtime()
     call barrier3(p, rank, comm)
     t1 = mpi_wtime()
     call mpi_barrier(comm, ierror)
     time(ktrial) = (t1-t0)*1.d6 ! time in microseconds
  enddo

  call mpi_reduce(time, max_time, ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
     print*, 'time to call bcast barrier is'
     avg = timestat(ntrial, max_time)
  endif

! 4. time the gather/scatter barrier

  call mpi_barrier(comm, ierror)

  do ktrial = 0, ntrial
     call random_number(flush) ! flush caches
     call mpi_barrier(comm, ierror)
     t0 = mpi_wtime()
     call barrier4(p, rank, comm)
     t1 = mpi_wtime()
     call mpi_barrier(comm, ierror)
     time(ktrial) = (t1-t0)*1.d6 ! time in microseconds
  enddo

  call mpi_reduce(time, max_time, ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
      print*, 'time to call gather/scatter barrier is'
      avg = timestat(ntrial, max_time)
  endif

! 5. time the mpi_isend/mpi_irecv barrier

  call mpi_barrier(comm, ierror)
  
  do ktrial = 0, ntrial
     call random_number(flush) ! flush caches
     call mpi_barrier(comm, ierror)
     t0 = mpi_wtime()
     call barrier5(p, rank, comm)
     t1 = mpi_wtime()
     call mpi_barrier(comm, ierror)
     time(ktrial) = (t1-t0)*1.d6 ! time in microseconds
  enddo

  call mpi_reduce(time, max_time, ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
      print*, 'time to call mpi_isend/mpi_irecv barrier is'
      avg = timestat(ntrial, max_time)
  endif

! 6. time the mpi_send/mpi_recv central manager barrier

  call mpi_barrier(comm, ierror)

  do ktrial = 0, ntrial
     call random_number(flush) ! flush caches
     call mpi_barrier(comm, ierror)
     t0 = mpi_wtime()
     call barrier6(p, rank, comm)
     t1 = mpi_wtime()
     call mpi_barrier(comm, ierror)
     time(ktrial) = (t1-t0)*1.d6 ! time in microseconds
  enddo

  call mpi_reduce(time, max_time, ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
      print*, 'time to call central-manager barrier using mpi_send/mpi_bcast is'
      avg = timestat(ntrial, max_time)
  endif

  call mpi_finalize(ierror)
END PROGRAM MPI_HW8

subroutine barrier1(p, rank, comm) ! using mpi sends and recvs
  use mpi
  implicit none
  integer            :: comm, p, rank, ierror, flag, i
  integer            :: status(mpi_status_size)
  flag = 0
  do i = 0, p-1
     if (i/=rank) then
        call mpi_send(flag, 1, mpi_int, i, 0, comm, ierror)
     endif
  enddo

  do i = 0, p-1
     if (i/=rank) then
        call mpi_recv(flag, 1, mpi_int, i, 0, comm, status, ierror)
     endif
  enddo

  return
end subroutine barrier1


subroutine barrier2(p, rank, comm) ! using mpi_alltoall
  use mpi
  implicit none
  integer            :: comm, p, rank, ierror, sendbuf(0:p-1), recvbuf(0:p-1)
  
  sendbuf(0:p-1) = 0
  call mpi_alltoall(sendbuf, 1, mpi_int, recvbuf, 1, mpi_int, comm, ierror)

  return
end subroutine barrier2

subroutine barrier3(p, rank, comm) ! using mpi_bcast
  use mpi
  implicit none
  integer            :: comm, p, rank, ierror, i, flag
  
  flag = 0
  
  call mpi_bcast(flag, 1, mpi_int, rank, comm, ierror)
  
  return 
end subroutine barrier3

subroutine barrier4(p, rank, comm) ! using mpi_gather/mpi_scatter
  use mpi
  implicit none
  integer           :: comm, p, rank, ierror, a(0:p-1), b
  
  a(0:p-1) = 0
  
  call mpi_scatter(a, 1, mpi_int, b, 1, mpi_int, 0, comm, ierror)  
  call mpi_gather(b, 1, mpi_int, a, 1, mpi_int, 0, comm, ierror)

  return
end subroutine barrier4

subroutine barrier5(p, rank, comm) ! using isend/irecv barrier
  use mpi
  implicit none
  integer           :: comm, p, rank, ierror, flag, i
  integer           :: req_arr(1:2*(p-1)) !count
  integer           :: status(mpi_status_size)

  flag = 0

  do i = 0, p-1
     if (i/=rank) then
        call mpi_isend(flag, 1, mpi_int, i, 0, comm, req_arr(1+i), ierror)
        call mpi_wait(req_arr(1+i), status, ierror)
     endif
  enddo

  do i = 0, p-1
     if (i/=rank) then
        call mpi_irecv(flag, 1, mpi_int, i, 0, comm, req_arr(p+i), ierror)
        call mpi_wait(req_arr(p+i), status, ierror)
     endif
  enddo

  return
end subroutine barrier5

subroutine barrier6(p, rank, comm) ! using rank 0 processor as a central manager
  use mpi
  implicit none
  integer          :: comm, p, rank, ierror, flag, i
  integer          :: status(mpi_status_size)

  flag = 0

  if (rank==0) then
      do i = 1, p-1
         call mpi_send(flag, 1, mpi_int, i, 0, comm, ierror)
      enddo
      call mpi_bcast(flag, 1, mpi_int, 0, comm, ierror)     
  else
      call mpi_recv(flag, 1, mpi_int, 0, 0, comm, status, ierror)
  endif

  return
end subroutine barrier6

! Summary

! Times in microseconds, p=16, and ntrial = 512
! time to call mpi_barrier is
! the median of t(1:n) is   3.09944152832031

! time to call mpi_send/mpi_recv barrier is
! the median of t(1:n) is   10.0135803222656

! time to call alltoall barrier is
! the median of t(1:n) is   13.1130218505859

! time to call bcast barrier is
! the median of t(1:n) is   1.19209289550781

! time to call gather/scatter barrier is
! the median of t(1:n) is   72.0024108886719

! time to call mpi_isend/mpi_irecv barrier is
! the median of t(1:n) is   77.9628753662109

! time to call central-manager barrier using mpi_send/mpi_bcast is
! the median of t(1:n) is   6.91413879394531

! Times in microseconds, p=16, and ntrial = 512
! time to call mpi_barrier is
! the median of t(1:n) is   13.1130218505859

! time to call mpi_send/mpi_recv barrier is
! the median of t(1:n) is   128.984451293945

! time to call alltoall barrier is
! the median of t(1:n) is   46.9684600830078

! time to call bcast barrier is
! the median of t(1:n) is   2.14576721191406

! time to call gather/scatter barrier is
! the median of t(1:n) is   197.887420654297

! time to call mpi_isend/mpi_irecv barrier is
! the median of t(1:n) is   513.076782226562

! time to call central-manager barrier using mpi_send/mpi_bcast is
! the median of t(1:n) is   30.9944152832031


