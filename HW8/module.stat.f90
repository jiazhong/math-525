MODULE STAT
CONTAINS
  FUNCTION Timestat(n, t)
  implicit none
  integer, intent(in)                  :: n
  double precision, intent(inout)      :: t(0:n)
  double precision                     :: ave, s(n)
  integer                              :: i
  double precision                     :: timestat

  print*, 't(0) = ', t(0)
  print*, 'the maximum of t(1:n) is', maxval(t(1:n))
  print*, 'the minimum of t(1:n) is', minval(t(1:n))
  
  ave = sum(t(1:n))/n
  timestat = ave
  
  print*, 'the average of t(1:n) is', ave

  do i = 1,n
     s(i) = (t(i)-ave)**2
  enddo

  print*, 'the standard deviation of t(1:n) is', sqrt(sum(s)/n)


 
  CALL Sort(t(1:n),n)
  if (mod(n,2)==1) then
     print*, 'the median of t(1:n) is', t((n+1)/2)
  else
     print*, 'the median of t(1:n) is', (t(n/2)+t(n/2+1))/2
  endif

  END FUNCTION Timestat



  INTEGER FUNCTION Findminimum(x, lo, hi)
     implicit none
     double precision, dimension(1:), intent(in)  ::  x
     integer, intent(in)                          :: lo, hi
     double precision                             :: minimum
     integer                                      :: location
     integer                                      :: i

     minimum = x(lo)
     location = lo
     Do i = lo+1, hi
        if (x(i) < minimum) THEN
           minimum = x(i)
           location = i
        endif
     enddo
     Findminimum = location
  END FUNCTION Findminimum

  SUBROUTINE Swap(a,b)
     implicit none
     double precision, intent(inout)  :: a, b
     double precision                 :: temp 
     
     temp = a
     a = b
     b = temp
  END SUBROUTINE Swap

  SUBROUTINE Sort(x, length)
     implicit none
     double precision, dimension(1:), intent(inout)  :: x
     integer, intent(in)                    :: length
     integer                                :: i
     integer                                :: location

     Do i = 1, length-1
        location = Findminimum(x,i,length)
        CALL Swap(x(i), x(location))
     enddo
  END SUBROUTINE Sort
 
END MODULE STAT

