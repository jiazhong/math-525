PROGRAM main

  use omp_lib
  use stat
  implicit none
  integer, parameter  :: n = 1024, ntrial = 128
  integer             :: i, j, ktrial
  double precision    :: t(0:ntrial), time(0:ntrial), t0, t1, avg, gflop
  double precision    :: A(n,n), x(n), y(n)
  double precision    :: y_loc(n)

  call random_number(A)
  call random_number(x)

! serial region 
  do ktrial = 0, ntrial
     y = 0.d0
     t0 = omp_get_wtime()
     y  = y + matmul(A,x)
     t1 = omp_get_wtime()
     time(ktrial) = t1-t0
  enddo
  avg   = timestat(ntrial, time)
  gflop = ((2.d0*dble(n)**2)/avg)/(1.d09)
  print*, 'the gflop is', gflop   



! parallel region
  do ktrial = 0, ntrial
     y = 0.d0
     y_loc = 0.d0
     t0 = omp_get_wtime()
!$OMP PARALLEL SHARED(x,y,A) PRIVATE(i,j,y_loc)
  !$OMP DO SCHEDULE(STATIC)
     do j = 1, n
        do i = 1, n
           y_loc(i) = y_loc(i)+A(i,j)*x(j)
        enddo
     enddo
  !$OMP ENDDO
  !$OMP CRITICAL 
     y=y+y_loc
  !$OMP END CRITICAL
!$OMP END PARALLEL
     t1 = omp_get_wtime()
     time(ktrial) = t1-t0
  enddo
  avg   = timestat(ntrial, time)
  gflop = ((2.d0*dble(n)**2)/avg)/(1.d09)
  print*, 'the gflop is', gflop


! parallel region
  do ktrial = 0, ntrial
     y = 0.d0
     y_loc = 0.d0
     t0 = omp_get_wtime()
!$OMP PARALLEL SHARED(x,y,A) PRIVATE(i,j,y_loc)
  !$OMP DO SCHEDULE(DYNAMIC)
     do j = 1, n
        do i = 1, n
           y_loc(i) = y_loc(i)+A(i,j)*x(j)
        enddo
     enddo
  !$OMP ENDDO
  !$OMP CRITICAL 
     y=y+y_loc
  !$OMP END CRITICAL
!$OMP END PARALLEL
     t1 = omp_get_wtime()
     time(ktrial) = t1-t0
  enddo
  avg   = timestat(ntrial, time)
  gflop = ((2.d0*dble(n)**2)/avg)/(1.d09)
  print*, 'the gflop is', gflop

! parallel region
  do ktrial = 0, ntrial
     y = 0.d0
     y_loc = 0.d0
     t0 = omp_get_wtime()
!$OMP PARALLEL SHARED(x,y,A) PRIVATE(i,j,y_loc)
  !$OMP DO SCHEDULE(GUIDED)
     do j = 1, n
        do i = 1, n
           y_loc(i) = y_loc(i)+A(i,j)*x(j)
        enddo
     enddo
  !$OMP ENDDO
  !$OMP CRITICAL 
     y=y+y_loc
  !$OMP END CRITICAL
!$OMP END PARALLEL
     t1 = omp_get_wtime()
     time(ktrial) = t1-t0
  enddo
  avg   = timestat(ntrial, time)
  gflop = ((2.d0*dble(n)**2)/avg)/(1.d09)
  print*, 'the gflop is', gflop

END PROGRAM main

!OUTPUT
! serial version
! the gflop is   4.54394990250474
 
! static version
! the gflop is   2.84283699663330
 
! dynamics version
! the gflop is   14.1505153814773
 
! guided version
! the gflop is   25.4866874964375

