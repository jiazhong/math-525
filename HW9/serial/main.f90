PROGRAM main

  use mpi
  use stat
  implicit none
  integer, parameter:: ntrial = 64
  integer           :: i, j, n, iter, niter, p, ktrial
  integer           :: ierror
  double precision  :: flush(3*1024*1024)
  double precision, allocatable  ::  A(:,:), B(:,:)
  double precision  :: t0,t1,avg, time(0:ntrial)

  call mpi_init(ierror)

  n = 4096
  niter = 50

  allocate(A(0:n+1,0:n+1))
  allocate(B(1:n,1:n))

  do ktrial = 0, ntrial

    call random_number(flush)
    t0 = mpi_wtime()

    do j = 0,n+1
       do i = 0,n+1
          A(i,j) = 0
       enddo
    enddo

    do i = 1,n
       A(i,0) = 1
    enddo
  
    do iter = 1,niter
       do j = 1,n
          do i = 1,n
             B(i,j) = 0.25d0*(A(i-1,j)+A(i+1,j)+A(i,j-1)+A(i,j+1))
          enddo
       enddo
       A(1:n,1:n) = B(1:n,1:n)
    enddo

    t1 = mpi_wtime()
    time(ktrial) = t1-t0
  enddo

  write(*, *)
  write(*, *) 'Matrix is'
  do i = 0, n+1
     do j = 0, n+1
        write(*, '(3f8.3)', advance='no'), A(i,j)
     enddo
     write(*, *)
  enddo 

  print*, 'time (serial verstion) is'
  avg = timestat(ntrial, time)

END PROGRAM main

! Output
! time in sec (serial verstion) is
! t(0) =    2.53180384635925
! the maximum of t(1:n) is   2.49155688285828
! the minimum of t(1:n) is   2.48808622360229
! the average of t(1:n) is   2.48955319076777
! the standard deviation of t(1:n) is  7.468850346604178E-004
! the median of t(1:n) is   2.48952543735504

