PROGRAM main_sendrecv

  use mpi
  use stat
  implicit none
  integer, parameter :: ntrial = 64
  integer            :: i, j, n, m, iter, niter, ktrial
  integer            :: ierror, p, rank, status(mpi_status_size)
  integer            :: right, left
  integer, parameter :: dp = mpi_double_precision, comm = mpi_comm_world
  double precision   :: flush(3*1024*1024)
  double precision, allocatable  :: A(:,:), B(:,:)
  double precision   :: t0,t1,avg, time(0:ntrial), max_time(0:ntrial)

  call mpi_init(ierror)
  call mpi_comm_size(comm, p, ierror)
  call mpi_comm_rank(comm, rank, ierror)

  n = 4096
  m = n/p
  if (rank<n-m*p) then
     m = m+1
  endif

  niter = 50

  ! initialize matrix
  allocate(A(0:n+1,0:m+1))
  allocate(B(1:n,1:m))

  if (rank==0) then
     do j = 0, m+1
        do i = 0, n+1
           A(i,j) = 0
        enddo
     enddo
     do i = 1, n
        A(i,0) = 1
     enddo

  else
    do j = 0, m+1
       do i = 0, n+1
          A(i,j) = 0
       enddo
    enddo
  endif

  ! left and right neighbours
  if (rank==0) then
     left = mpi_proc_null
  else
     left = rank-1
  endif

  if (rank==p-1) then
     right = mpi_proc_null
  else
     right = rank+1
  endif

  do ktrial = 0, ntrial
     call random_number(flush)
     call mpi_barrier(comm, ierror)

     t0 = mpi_wtime()

     ! Jacobi iteration
     do iter = 1, niter

        do j = 1, m
           do i = 1, n
              B(i,j) = 0.25d0*(A(i-1,j)+A(i+1,j)+A(i,j-1)+A(i,j+1))
           enddo
        enddo
        A(1:n,1:m) = B(1:n,1:m)

        call mpi_sendrecv(B(1,1), n, dp, left, 0, A(1,m+1), n, dp, right, 0, comm, status, ierror)
        call mpi_sendrecv(B(1,m), n, dp, right, 0, A(1,0), n, dp, left, 0, comm, status, ierror)
     enddo
     t1 = mpi_wtime()
     time(ktrial) = t1-t0
  enddo

  call mpi_reduce(time(0), max_time(0), ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
     avg = timestat(ntrial, max_time)
  endif

  call mpi_finalize(ierror)
END PROGRAM main_sendrecv

! Ave T for 16 processors
! the average of t(1:n) is  0.525699835270643

! Ave T for 32 processors
! the average of t(1:n) is  0.266128368675709

! Ave T for 64 processors
! the average of t(1:n) is  0.127718307077885

