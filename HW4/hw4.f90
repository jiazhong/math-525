PROGRAM hw4_2015

use stat
use omp_lib
implicit none
integer, parameter       :: n = 1024, ntrial = 100 ! # of timing trials
integer                  :: ktrial
double precision         :: t0, t1, time(0:ntrial), A(n,n), B(n,n), C(n,n)
double precision         :: av_noflushing, av_flushing, gflops
double precision         :: flush(3*1024*1024)  ! flush a 20 MB cache

call random_number(A)
call random_number(B)
call random_number(C)

do ktrial = 0, ntrial

   t0 = omp_get_wtime()

   !C = C + matmul(A,B)
   call dgemm('n','n',n,n,n,1.d0,A,n,B,n,1.d0,C,n)

   t1 = omp_get_wtime()

   time(ktrial) = t1 - t0 ! time in seconds
enddo

   av_flushing = timestat(ntrial, time)


call random_number(A)
call random_number(B)
call random_number(C)
call random_number(flush)

do ktrial = 0, ntrial
   flush = flush + 0.001d0
   
   t0 = omp_get_wtime()

   call dgemm('n','n',n,n,n,1.d0,A,n,B,n,1.d0,C,n)

   t1 = omp_get_wtime()

   time(ktrial) = t1 - t0
enddo

   av_noflushing = timestat(ntrial, time)
   

print*, 'n=',n
print*, 'av_flushing=',av_flushing
print*, 'av_noflushing=',av_noflushing
print*, 'Ratio (flushing)/(no flushing) =',av_flushing/av_noflushing


END PROGRAM hw4_2015

! OUTPUT

! n=           8
! Ratio (flushing)/(no flushing) =   7.42473118279570

! n=          16
! Ratio (flushing)/(no flushing) =   5.82953131480713

! n=          32
! Ratio (flushing)/(no flushing) =   2.55050505050505

! n=          64
! Ratio (flushing)/(no flushing) =   2.13564206515899

! n=         128
! Ratio (flushing)/(no flushing) =   1.22336642324661

! n=         256
! Ratio (flushing)/(no flushing) =   1.00904898013993

! n=         512
! Ratio (flushing)/(no flushing) =  0.978457387222406

! n=        1024
! Ratio (flushing)/(no flushing) =  0.998853427075195

