PROGRAM hw2_2015

use stat
use omp_lib

implicit none
integer, parameter              :: ntrial = 100 !  of timing trials
double precision, parameter     :: n = 1024      !  and also 128 and 512
integer                         :: ktrial
double precision                :: t0, t1, time(0:ntrial), A(n,n), B(n,n), C(n,n)
double precision                :: ave, gflop

call random_number(A)
call random_number(B)
call random_number(C)

do ktrial = 0, ntrial
   t0 = omp_get_wtime()

   C = C + matmul(A,B)
!  call dgemm('n','n',n,n,n,1.d0,A,n,B,n,1.d0,C,n)

   t1 = omp_get_wtime()

   time(ktrial) = t1 - t0  ! time in seconds
enddo

   ave =  timestat(ntrial, time)
   gflop = ((2*n**3)/ave)/(10**9)

print*,'the gflop is',gflop

END PROGRAM hw2_2015

! n = 256
! t(0) =   0.723341666853073
! the maximum of t(1:n) is  9.215116500854492E-003
! the minimum of t(1:n) is  8.193969726562500E-003
! the average of t(1:n) is  8.213739395141601E-003
! the standard deviation of t(1:n) is  1.008548785449043E-004
! the median of t(1:n) is  8.199572563171387E-003
! the gflop is   4.08515907137830

! n = 512
! t(0) =   0.723341666853073
! the maximum of t(1:n) is  1.010608673095703E-002
! the minimum of t(1:n) is  8.208990097045898E-003
! the average of t(1:n) is  8.236999511718751E-003
! the standard deviation of t(1:n) is  1.879593504513397E-004
! the median of t(1:n) is  8.220553398132324E-003
! the gflop is   4.07362316244674

! n = 1024
! t(0) =   0.268903198585273
! the maximum of t(1:n) is  0.522181034088135
! the minimum of t(1:n) is  0.520560026168823
! the average of t(1:n) is  0.520852096080780
! the standard deviation of t(1:n) is  2.131826506549877E-004
! the median of t(1:n) is  0.520829439163208
! the gflop is   4.12302007452600


