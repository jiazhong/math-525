MODULE STAT
CONTAINS
  FUNCTION TIMESTAT(n, t)
  implicit none
  integer, intent(in)               :: n
  double precision, intent(in)      :: t(n)
  double precision                  :: ave, s(n)
  integer                           :: i
  double precision                  :: timestat

  print*, 't(0) = ', t(0)
  print*, 'the maximum of t(1:n) is', maxval(t(1:n))
  print*, 'the minimum of t(1:n) is', minval(t(1:n))
  
  ave = sum(t(1:n))/n
  timestat = ave
  
  print*, 'the average of t(1:n) is', ave

  do i = 1,n
     s(i) = (t(i)-ave)**2
  enddo

  print*, 'the standard deviation of t(1:n) is', sqrt(sum(s)/n)
  
  if (mod(n,2)==1) then
     print*, 'the median of t(1:n) is', t((n+1)/2)
  else
     print*, 'the median of t(1:n) is', (t(n/2)+t(n/2+1))/2
  endif

  END FUNCTION TIMESTAT

END MODULE STAT

