!  Computer y = y + Ax
   
   implicit none
   integer , parameter  :: n = 6 ! for print visuliazation using 6
                                 ! for HW checking using 137
   double precision     :: y_init(n), y(n), y_check(n), A(n,n), x(n), error
   integer              :: i, j   

   call random_number(A)
   call random_number(x)
   call random_number(y)
   y_init = y
   
!  Perform calculations
   y_check = y_init + matmul(A,x)
    
   do i = 1,n
      do j = 1,n
         y(i) = y(i) + A(i,j)*x(j)
      enddo
   enddo

   error = maxval(abs(y - y_check))

!  print A: good visuliaztion when n is small
   write(*, *)
   write(*, *) 'Matrix A:'
   do i = 1,n
      do j = 1,n
         write(*, '(3f8.3)', advance='no'),A(i,j)
      enddo
      write(*,*)
   enddo

!  print x: good visuliaztion when n is small
   write(*, *)
   write(*, *) 'Vector x:'
   do i = 1,n
      write(*, '(3f8.3)', advance='no'),x(i)
      write(*, *)
   enddo

!  print y: good visuliazation when n is small
   write(*, *)
   write(*, *) 'Vector y:'
   do i = 1,n
      write(*, '(3f8.3)', advance='no'),y(i)
      write(*, *)
   enddo

!  print y_intrinsic: good visuliazation when n is small
   write(*, *)
   write(*, *) 'Vector y_check:'
   do i = 1,n
      write(*, '(3f8.3)', advance='no'),y_check(i)
      write(*, *)
   enddo

!  print maximum error 
   write(*, *)
   print*, 'the maximum error is', error
   end

! Result

! Matrix A:
!   0.000   0.335   0.090   0.908   0.076   0.995
!   0.025   0.915   0.888   0.098   0.912   0.778
!   0.353   0.796   0.701   0.040   0.092   0.020
!   0.667   0.833   0.735   0.085   0.638   0.170
!   0.963   0.345   0.300   0.559   0.852   0.995
!   0.838   0.871   0.050   0.926   0.121   0.754
 
! Vector x:
!   0.218
!   0.659
!   0.539
!   0.480
!   0.839
!   0.025
 
! Vector y:
!   1.784
!   2.505
!   1.843
!   2.115
!   2.318
!   2.144
 
! Vector y_check:
!   1.784
!   2.505
!   1.843
!   2.115
!   2.318
!   2.144
 
! the maximum error is  4.440892098500626E-016

