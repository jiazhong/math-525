!  Computers the sum of 1+2+...+n where n is declared as integer,
!  parameter  :: n = 137 

   implicit none ! 
   integer , parameter  :: n = 137
   integer     :: x(n), s, s_formula, s_intrinsic, error1, error2
   integer              :: i

!  initialize sum, sum_formula, sum_intrinsic
   s = 0
   s_formula = 0
   s_intrinsic = 0

!  initialize array x(n)
   do i = 1,n
      x(i) = i
   enddo

!  perform calculations
   do i = 1,n
      s = s + x(i)
   enddo

   s_formula = n*(n+1)/2 ! calculate sum by formula n*(n+1)/2
 
   s_intrinsic = SUM(x)  ! calculate sum by sum intrinsic function

   error1 = abs(s-s_formula)

   error2 = abs(s-s_intrinsic)

!  print
   print*, '(a)'
   print*, 'sum= ', s
   print*, 'sum_formula= ', s_formula
   print*, 'The absolute error is ', error1

   print*, '(b)'
   print*, 'sum= ', s
   print*, 'sum_intrinsic= ', s_intrinsic
   print*, 'The absolute error is ', error2
   end


! result

! (a)
! sum=         9453
! sum_formula=         9453
! The absolute error is            0
! (b)
! sum=         9453
! sum_intrinsic=         9453
! The absolute error is            0

