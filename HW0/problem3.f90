!  Computer C = C + AB

   implicit none
   integer , parameter              :: n = 3 ! for print visualization using 3
                                             ! for HW checking using 21
   double precision, DIMENSION(n,n) :: A, B, C, C_check
   double precision                 :: error = 0, error_temp
   integer                          :: i, j, k

   call random_number(A)
   call random_number(B)
   call random_number(C)

!  Perform calculations
   C_check = C + matmul(A,B)

   do i = 1,n
      do j = 1,n
         do k = 1,n
            C(i,j) = C(i,j) + A(i,k)*B(k,j)
         enddo
      enddo
   enddo

   error_temp =maxval(abs(C - C_check))

!  print matrix A: good visualization for small n 
   write(*, *)
   write(*, *) 'Matrix A:'
   do i = 1,n
      do j = 1,n
         write(*, '(3f8.3)', advance='no'), A(i,j)
      enddo
      write(*, *)
   enddo

!  print matrix B: good visualization for small n
   write(*, *)
   write(*, *) 'Matrix B:' 
   do i = 1,n 
      do j = 1,n
         write(*, '(3f8.3)', advance='no'), B(i,j)
      enddo
      write(*, *)
   enddo

!  print matrix C: good visualization for small n
   write(*, *)
   write(*, *) 'Matrix C:'
   do i =1,n
      do j = 1,n
         write(*, '(3f8.3)', advance='no'), C(i,j)
      enddo
      write(*, *)
   enddo

!  print matrix C_check: good visualization for small n
   write(*, *)
   write(*, *) 'Matrix C_check'
   do i =1,n
      do j = 1,n
         write(*, '(3f8.3)', advance='no'), C_check(i,j)
      enddo
      write(*, *)
   enddo

!  print the maximum abosulte error
   write(*, *)
   print*, 'The maximum abosulte error is', error
   end

! Result
! Matrix A:
!   0.000   0.667   0.335
!   0.025   0.963   0.915
!   0.353   0.838   0.796
 
! Matrix B:
!   0.833   0.090   0.735
!   0.345   0.888   0.300
!   0.871   0.701   0.050
 
! Matrix C:
!   1.430   0.913   0.293
!   1.249   2.058   1.265
!   1.316   2.261   0.642
 
! Matrix C_check
!   1.430   0.913   0.293
!   1.249   2.058   1.265
!   1.316   2.261   0.642
 
! The maximum abosulte error is  0.000000000000000E+000



