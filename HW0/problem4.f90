! Sum the elements of double precisiou array x

  implicit none
  integer, parameter :: n=5
  double precision   :: x(n), s, s_check, error
  integer            :: i

! initialize sum, sum_check
  s = 0
  s_check = 0

! initialize array x(n) by random_number function
  call random_number(x)

! perform calculations
  do i = 1,n
     s = s + x(i)
  enddo

  s_check = SUM(x) 

! calcualte abosulte error
  error = abs(s-s_check)

! print
  print*, 'sum=', s
  print*, 'sum_check=', s_check
  print*, 'The absolute error is', error

  end

! Result

! sum=   2.00796700952444     
! sum_check=   2.00796700952444     
! The absolute error is  0.000000000000000E+000

