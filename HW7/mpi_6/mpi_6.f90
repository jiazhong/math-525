PROGRAM MPI_HW6

  use mpi
  use stat
  implicit none
  integer, parameter  :: n = 1024*1024, ntrial = 64
  ! n = 1, ntrial = 1024
  ! n = 2048, ntrial = 128
  ! n = 1024*1024, ntrial = 64
  double precision    :: flush(3*1024*1024) ! to flushe an 8*3 MB cache
  integer, parameter  :: dp = mpi_double_precision, comm = mpi_comm_world
  double precision    :: t0, t1, time(0:ntrial), max_time(0:ntrial)
  double precision    :: x, A(n), B(n), avg
  integer             :: i, j, ierror, left, right
  integer             :: p, rank, status(mpi_status_size), ktrial
  
  call mpi_init(ierror)
  call mpi_comm_size(comm, p, ierror)
  call mpi_comm_rank(comm, rank, ierror)

  call random_number(A)
  call random_number(B)

  ! Part A: time the sendrecv version   
  do ktrial = 0, ntrial

     flush = flush + 0.001d0     

     call mpi_barrier(comm, ierror)

     if (rank==0) then
        t0 = mpi_wtime()

        call mpi_sendrecv(A(1), n, dp, p-1, 0, B(1), n, dp, p-1, 0, comm, status, ierror)
        
        t1 = mpi_wtime()

        time(ktrial) = (t1-t0)
     else if (rank==p-1) then
        call mpi_sendrecv(B(1), n, dp, 0, 0, A(1), n, dp, 0, 0, comm, status, ierror)
     endif
 
  enddo
        
  !call mpi_reduce(time(0), max_time(0), ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
     print*, ''
     print*, 'Time in seconds'
     print*, 'Number of process', p
     print*, 'Number of timings=', ntrial
     avg = timestat(ntrial, time)
     print*, ''
  endif

  ! PART B: time the send and receive version when there are no deadlocks
  do ktrial = 0, ntrial

     flush = flush + 0.001d0
 
     call mpi_barrier(comm, ierror)

     if (rank==0) then
        t0 = mpi_wtime()

        call mpi_send(A(1), n, dp, p-1, 0, comm, ierror)
        call mpi_recv(A(1), n, dp, p-1, 0, comm, status, ierror)

        t1 = mpi_wtime()

        time(ktrial) = (t1-t0)
     else if (rank==p-1) then
        call mpi_recv(B(1), n, dp, 0, 0, comm, status, ierror)
        call mpi_send(B(1), n, dp, 0, 0, comm, ierror)
     endif

  enddo

  !call mpi_reduce(time(0), max_time(0), ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
     print*, ''
     print*, 'Time in seconds'
     print*, 'Number of process', p
     print*, 'Number of timings=', ntrial
     avg = timestat(ntrial, time)
     print*, ''
  endif

  call mpi_finalize(ierror)
END PROGRAM MPI_HW6

! OUTPUT SUMMARY
! p    n         sendrecv                  send & recv
! 16   1         9.990995749831200E-006    2.034800127148628E-005
! 16   2048      3.241188824176788E-005    5.230866372585297E-005
! 16   1024**2   3.205612301826477E-005    5.329586565494537E-005
! 32   1         1.084082759916782E-005    2.135848626494408E-005
! 32   2048      9.270012378692627E-005    1.348331570625305E-004
! 32   1024**2   8.732266724109650E-005    1.318622380495071E-004
