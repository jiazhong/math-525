PROGRAM MPI_HW5

  use mpi
  use stat
  implicit none
  integer, parameter :: n = 64, ntrial = 128
  double precision   :: flush(3*1024*1024) 
  integer, parameter :: dp = mpi_double_precision, comm = mpi_comm_world
  double precision   :: t0, t1, time(0:ntrial), timelocal(0:ntrial), avg
  double precision   :: x, A(n,n), B(n,n), C(n,n), D(n,n)
  integer            :: i, j, ierror, left, right
  integer            :: p, rank, status(mpi_status_size), ktrial
  
  call mpi_init(ierror)
  call mpi_comm_size(comm, p, ierror)
  call mpi_comm_rank(comm, rank, ierror)

  flush = flush + 0.001d0

  call random_number(A)
  A = A + dble(rank)
 
  if (rank==0) then
     left = p-1
  else 
     left = rank-1
  endif

  if (rank==p-1) then
     right = 0
  else
     right = rank+1
  endif

  do ktrial = 0, ntrial
     
     call mpi_barrier(comm, ierror)
     
     t0 = mpi_wtime()
     
     call mpi_sendrecv(A(1,1), n*n, dp, right, 0, B(1,1), n*n, dp, left, 0, comm, status, ierror)

     C = matmul(A,B)

     call mpi_sendrecv(C(1,1), n*n, dp, left, 0, D(1,1), n*n, dp, right, 0, comm, status, ierror)

     t1 = mpi_wtime()
     timelocal(ktrial) = (t1-t0)
  enddo

  call mpi_reduce(timelocal(0), time(0), ntrial+1, dp, MPI_MAX, 0, comm, ierror)

  if (rank==0) then
     print*,''
     print*, 'All taks have been completed'
     print*, ''
     print*, 'Time in seconds' 
     print*, 'Number of processor', p
     print*, 'Number of timings = ', ntrial
     avg = timestat(ntrial,time)
     print*, ''
  endif
 
  call mpi_finalize(ierror)

END PROGRAM MPI_HW5

! OUTPUT

! After replacing mpi_sends with mpi_ssends, we would have a deadlocks

! All taks have been completed

! Time in seconds
! Number of processor          16
! Number of timings =          128
! t(0) =   5.569458007812500E-004
! the maximum of t(1:n) is  1.652240753173828E-004
! the minimum of t(1:n) is  1.049041748046875E-004
! the average of t(1:n) is  1.087263226509094E-004
! the standard deviation of t(1:n) is  6.443434233212218E-006
! the median of t(1:n) is  1.070499420166016E-004


! All taks have been completed

! Time in seconds
! Number of processor          32
! Number of timings =          128
! t(0) =   5.528926849365234E-004
! the maximum of t(1:n) is  1.938343048095703E-004
! the minimum of t(1:n) is  1.258850097656250E-004
! the average of t(1:n) is  1.390110701322556E-004
! the standard deviation of t(1:n) is  1.015626718024900E-005
! the median of t(1:n) is  1.370906829833984E-004


! All taks have been completed

! Time in seconds
! Number of processor          64
! Number of timings =          128
! t(0) =   4.680156707763672E-004
! the maximum of t(1:n) is  1.840591430664062E-004
! the minimum of t(1:n) is  1.389980316162109E-004
! the average of t(1:n) is  1.486912369728088E-004
! the standard deviation of t(1:n) is  6.789400590779382E-006
! the median of t(1:n) is  1.471042633056641E-004

