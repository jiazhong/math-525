MODULE timegen
CONTAINS

SUBROUTINE timegeneration

  use globaldef
  call para
  n_t        = (time_end-time_start)/dt+1
  dt_adi     = dt/2
  n_t_adi    = n_t*2-1

  allocate(t(n_t));
  allocate(t_adi(n_t_adi));

  do i = 1,n_t
     t(i) = time_start+(i-1)*dt
  enddo

  do i = 1,n_t_adi
     t_adi(i) = time_start+(i-1)*dt_adi
  enddo

END SUBROUTINE

END MODULE timegen
