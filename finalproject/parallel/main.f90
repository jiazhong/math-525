PROGRAM main

  use mpi
  use functions 
  use globaldef
  use gridgen
  use timegen
  use paragen
  use solver

  implicit none

  pi = 4d0*atan(1d0)

  call mpi_init(ierror)
  call mpi_comm_size(comm, p, ierror)
  call mpi_comm_rank(comm, rank, ierror)
  call gridgeneration
  call timegeneration
  call parageneration

  n_yloc = int((n_y-2)/(p-1))
  n_yrem = mod((n_y-2),(p-1))

  n_x_spherloc = int((n_x_spher-2)/(p/2-1))
  n_x_spherrem = mod((n_x_spher-2),(p/2-1))

  n_x_fldloc = int((n_x_fld-1)/(p/2-1))
  n_x_fldrem = mod((n_x_fld-1),(p/2-1))

  allocate(Z(n_y, final, n_t_adi))
  allocate(Z_xsend1(final))
  allocate(Z_xsend2(n_yloc+n_yrem, final))
  allocate(Z_xsend3(final))
  allocate(Z_xsend4(n_yloc, final))
  allocate(Z_xsend5(n_y,final))
  !allocate(Z_xsend5(n_y, final))
  !allocate(Z_xrecv5(n_y, final))
  allocate(Z_ysend1(n_y))
  allocate(Z_ysend2(n_y))
  allocate(Z_ysend3(n_y))
  allocate(Z_ysend4(n_y, n_x_spherloc))
  allocate(Z_ysend5(n_y, n_x_fldloc))
  allocate(Z_ysend6(n_y,final))
  allocate(Z_ysend7(n_y, n_x_spherloc+n_x_spherrem))
  allocate(Z_ysend8(n_y, n_x_fldloc+n_x_fldrem))

  allocate(Sub_1(final))
  allocate(Diag_1(final))
  allocate(Sup_1(final-1))
  allocate(B_1(final))

  allocate(Sub_2(n_y))
  allocate(Diag_2(n_y))
  allocate(Sup_2(n_y-1))
  allocate(B_2(n_y))

  do j = 1,n_y
     do i = 1,prt
        Z(j,i,1) = 1
     enddo
  enddo

  t0 = mpi_wtime()

  do k = 2,n_t_adi
     if (mod(k,2)==0) then
        call mpi_barrier(comm, ierror)
        if (rank==0) then
           call mpi_recv(Z(1,:,k), final, dp, p-1, 0, comm, status, ierror)
           call mpi_recv(Z((2+(p-2)*n_yloc):(1+(p-1)*n_yloc+n_yrem),:,k), (n_yloc+n_yrem)*final, dp, p-1, 0, comm, status, ierror)
           call mpi_recv(Z(n_y,:,k), final, dp, p-1, 0, comm, status, ierror)        
           do j = 1,p-2
              call mpi_recv(Z((2+(j-1)*n_yloc):(1+j*n_yloc),:,k), n_yloc*final, dp, j, 0, comm, status, ierror)
           enddo
           do i = 1, n_y
              do j = 1, final
                 Z_xsend5(i,j) = Z(i,j,k)
              enddo
           enddo
           do i = 1, p-1
              call mpi_send(Z_xsend5(1,1), n_y*final, dp, i, 0, comm, ierror)
           enddo
        elseif (rank==p-1) then
           call load_xmatrix1
           Z_xsend1 = TRID(Sub_1,Diag_1,Sup_1,B_1,final)
           call mpi_send(Z_xsend1(1), final, dp, 0, 0, comm, ierror)

           do j_loc = 1,n_yloc+n_yrem
              j = 1 + j_loc + (rank-1)*n_yloc
              call load_xmatrix2
              Z_xsend2(j_loc,:) = TRID(Sub_1,Diag_1,Sup_1,B_1,final)
           enddo
           call mpi_send(Z_xsend2(1,1), (n_yloc+n_yrem)*final, dp, 0, 0, comm, ierror)

           call load_xmatrix3
           Z_xsend3 = TRID(Sub_1,Diag_1,Sup_1,B_1,final)
           call mpi_send(Z_xsend3(1), final, dp, 0, 0, comm, ierror)

           call mpi_recv(Z(1,1,k), n_y*final, dp, 0, 0, comm, status, ierror)
        else
           do j_loc = 1,n_yloc
              j = 1 + j_loc + (rank-1)*n_yloc
              call load_xmatrix2
              Z_xsend4(j_loc,:) = TRID(Sub_1,Diag_1,Sup_1,B_1,final)
           enddo
           call mpi_send(Z_xsend4(1,1), n_yloc*final, dp, 0, 0, comm, ierror)

           call mpi_recv(Z(1,1,k), n_y*final, dp, 0, 0, comm, status, ierror)
        endif

     else
        call mpi_barrier(comm, ierror)
        if (rank==0) then
           call mpi_recv(Z(:,1,k), n_y, dp, p-1, 0, comm, status, ierror)
           call mpi_recv(Z(:,prt,k), n_y, dp, p-1, 0, comm, status, ierror)
           call mpi_recv(Z(:,final,k), n_y, dp, p-1, 0, comm, status, ierror)

           call mpi_recv(Z(:,1+1+((p-2)/2)*n_x_spherloc,k), n_y*n_x_spherrem, dp, p-1, 0, comm, status, ierror)
           call mpi_recv(Z(:,prt+1+((p-2)/2)*n_x_fldloc,k), n_y*n_x_fldrem, dp, p-1, 0, comm, status, ierror)

           do i = 1,(p-2)/2
              call mpi_recv(Z(:,(1+1+(i-1)*n_x_spherloc):(1+i*n_x_spherloc),k), n_y*n_x_spherloc, dp, i, 0, comm, status, ierror)
           enddo
           do i = (p-2)/2+1,p-2
              call mpi_recv(Z(:,(prt+1+(i-(p-2)/2-1)*n_x_fldloc):(prt+(i-(p-2)/2)*n_x_fldloc),k), n_y*n_x_fldloc, dp, i, 0, comm, status, ierror)
           enddo

           do i = 1, n_y
              do j = 1, final
                 Z_ysend6(i,j) = Z(i,j,k)
              enddo
           enddo
           do i = 1, p-1
              call mpi_send(Z_ysend6(1,1), n_y*final, dp, i, 0, comm, ierror)
           enddo
        elseif (rank==p-1) then
           Z_ysend1 = Z(:,1,k-1)
           call mpi_send(Z_ysend1(1), n_y, dp, 0, 0, comm, ierror)
           Z_ysend2 = Z(:,prt,k-1)
           call mpi_send(Z_ysend2(1), n_y, dp, 0, 0, comm, ierror)
           Z_ysend3 = Z(:,final,k-1) 
           call mpi_send(Z_ysend3(1), n_y, dp, 0, 0, comm, ierror)

           do i_loc = 1, n_x_spherrem
              i = 1 + i_loc + ((p-2)/2)*n_x_spherloc
              call load_ymatrix1
              Z_ysend7(:,i_loc) = TRID(Sub_2,Diag_2,Sup_2,B_2,n_y)
           enddo
           call mpi_send(Z_ysend7(1,1), n_y*n_x_spherrem, dp, 0, 0, comm, ierror)
           do i_loc = 1, n_x_fldrem
              i = prt + ((p-2)/2)*n_x_fldloc 
              call load_ymatrix2
              Z_ysend8(:,i_loc) = TRID(Sub_2,Diag_2,Sup_2,B_2,n_y)
           enddo
           call mpi_send(Z_ysend8(1,1), n_y*n_x_fldrem, dp, 0, 0, comm, ierror)

           call mpi_recv(Z(1,1,k), n_y*final, dp, 0, 0, comm, status, ierror)
        elseif (rank<=((p-2)/2) .and. rank>=1 ) then
           do i_loc = 1, n_x_spherloc
              i = 1 + i_loc + (rank-1)*n_x_spherloc
              call load_ymatrix1
              Z_ysend4(:,i_loc) = TRID(Sub_2,Diag_2,Sup_2,B_2,n_y)
           enddo
           call mpi_send(Z_ysend4(1,1), n_y*n_x_spherloc, dp, 0, 0, comm, ierror)

           call mpi_recv(Z(1,1,k), n_y*final, dp, 0, 0, comm, status, ierror)
        else
           do i_loc = 1, n_x_fldloc
              i = prt + i_loc + (rank-(p-2)/2-1)*n_x_fldloc
              call load_ymatrix2
              Z_ysend5(:,i_loc) = TRID(Sub_2,Diag_2,Sup_2,B_2,n_y)
           enddo
           call mpi_send(Z_ysend5(1,1), n_y*n_x_fldloc, dp, 0, 0, comm, ierror)

           call mpi_recv(Z(1,1,k), n_y*final, dp, 0, 0, comm, status, ierror)
        endif

        !if (rank==0) then
        !   Z(:,1,k) = Z(:,1,k-1)
        !   do i = 2,prt-1
        !      call load_ymatrix1
        !      Z(:,i,k) = TRID(Sub_2,Diag_2,Sup_2,B_2,n_y)
        !   enddo
        !   Z(:,prt,k) = Z(:,prt,k-1)
        !   do i = prt+1,final-1
        !      call load_ymatrix2
        !      Z(:,i,k) = TRID(Sub_2,Diag_2,Sup_2,B_2,n_y)
        !   enddo 
        !   Z(:,final,k) = Z(:,final,k-1)
        !   do i = 1, n_y
        !      do j = 1, final
        !      Z_xsend5(i,j) = Z(i,j,k)
        !      enddo
        !   enddo
        !   do i = 1,p-1
        !      call mpi_send(Z_xsend5(1,1), n_y*final, dp, i, 0, comm, ierror)
        !   enddo
        !else
        !   call mpi_recv(Z_xrecv5(1,1), n_y*final, dp, 0, 0, comm, status, ierror)
        !   do i = 1, n_y
        !      do j = 1, final
        !         Z(i,j,k) = Z_xrecv5(i,j)
        !      enddo
        !   enddo
        !endif
     endif
  enddo     

  t1 = mpi_wtime()
  time = t1-t0
  call mpi_reduce(time,time_max,1,dp, MPI_MAX, 0, comm, ierror)

  call mpi_barrier(comm, ierror)
  if (rank==0) then
     print*, 'time=', time_max
  !   open(unit=10,file='output.dat')
  !   write(10,*) 'n_x', final, 'n_y', n_y, 'n_t', n_t
  !   do k = 1, n_t
  !      do i = 1, n_y
  !         do j = 1, final
  !            write(10, '(E16.8\)') R(i,j)
  !            write(10, '(E16.8\)') theta(i,j)
  !            write(10, '(E16.8)') Z(i,j,2*(k-1)+1)
  !         enddo
  !      enddo
  !   enddo
  endif
  call mpi_finalize(ierror) 
END PROGRAM main
