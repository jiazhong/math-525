MODULE globaldef
  use mpi
  implicit none
  integer           :: i, j, k, n_x_spher, n_x_fld, final, n_y, prt
  double precision  :: x0_spher, xh_spher, dx_spher, x0_fld, xh_fld, dx_fld
  double precision  :: y0, yh, dy, pi
  integer           :: n_t, n_t_adi
  double precision  :: time_start, time_end, dt, dt_adi
  double precision  :: k1, k2, c1, c2, rho1, rho2, alpha1, alpha2, F1, F2, Fc
  double precision  :: tmp1, tmp2, tmp3
  double precision  :: U, Pe ! inflow velocity
  integer           :: ierror, p, rank, status(mpi_status_size)
  integer, parameter:: dp = mpi_double_precision, comm = mpi_comm_world
  double precision  :: t0, t1, time, time_max

  double precision, allocatable :: x(:), y(:), R(:,:), theta(:,:)
  double precision, allocatable :: t(:), t_adi(:)
  double precision, allocatable :: Z(:,:,:)
  double precision, allocatable :: Sub_1(:), Diag_1(:), Sup_1(:), B_1(:)
  double precision, allocatable :: Sub_2(:), Diag_2(:), Sup_2(:), B_2(:)

  integer           :: n_yloc, n_yrem, j_loc, n_x_spherloc, n_x_spherrem, n_x_fldloc, n_x_fldrem, i_loc
  double precision, allocatable :: Z_xsend1(:), Z_xsend2(:,:), Z_xsend3(:), Z_xsend4(:,:), Z_xsend5(:,:)
  double precision, allocatable :: Z_ysend1(:), Z_ysend2(:),   Z_ysend3(:), Z_ysend4(:,:), Z_ysend5(:,:), Z_ysend6(:,:)
  double precision, allocatable :: Z_ysend7(:,:), Z_ysend8(:,:)
  double precision, allocatable :: Z_xrecv1(:), Z_xrecv2(:,:), Z_xrecv3(:), Z_xrecv4(:,:), Z_xrecv5(:,:)

CONTAINS
SUBROUTINE para
! grid
  x0_spher = 0d0
  xh_spher = 1d0
  n_x_spher = 200

  x0_fld = xh_spher  
  xh_fld = 10d0
  n_x_fld = 200

  y0 = 0
  yh = pi
  n_y = 200

! time
  time_start = 0d0
  time_end = 1d0
  dt = 0.001d0

! themral properties
  k1 = 54d0
  c1 = 452d0
  rho1 = 8000d0

  k2 = 0.6d0
  c2 = 4.1813d0
  rho2 = 1000d0

! velocity
  U = 0d0

END SUBROUTINE para

END MODULE globaldef
