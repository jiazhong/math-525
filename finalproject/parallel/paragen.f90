MODULE paragen
CONTAINS

SUBROUTINE parageneration

  use globaldef  
  call para

  alpha1 = k1/(c1*rho1)

  alpha2 = k2/(c2*rho2)

  F1     = (alpha1/alpha2)*(dt/dx_spher**2)
  F2     = dt/dx_fld**2

  Pe     = (0.1*U)/alpha2
  Fc     = (Pe/4)*(dt_adi/dx_fld)

END SUBROUTINE parageneration

END MODULE paragen
