MODULE gridgen
CONTAINS

SUBROUTINE gridgeneration

  use globaldef
  call para 

  dx_spher  = (xh_spher-x0_spher)/(n_x_spher-1)
  x0_fld    = xh_spher
  dx_fld    = (xh_fld-x0_fld)/(n_x_fld-1)
  dy  = (yh-y0)/(n_y-1)
  prt   = n_x_spher
  final = prt+n_x_fld
  
  allocate(x(final))
  allocate(y(n_y))
  allocate(R(n_y, final))
  allocate(theta(n_y,final))

  do i = 1,prt
     x(i) = x0_spher+(i-1)*dx_spher
  enddo

  do i = 1,n_x_fld
     x(i+prt) = x0_fld+(i-1)*dx_fld
  enddo

  !call map_spher(x(1:n_x_spher),n_x_spher)
  !x(1:n_x_spher) = (xh_spher-x0_spher)*(x(1:n_x_spher)-x(1))&
  !                 /(x(prt)-x(1))+x0_spher
  !call map_fld(x(prt:final),n_x_fld)
  !x(prt:final) = (xh_fld-x0_fld)*(x(prt:final)-x(prt))/(x(final)-x(prt))+x0_fld

  do i = 1,n_y
     y(i) = y0+(i-1)*dy
  enddo

  do i = 1,n_y
     do j = 1, final
        R(i,j)     = x(j)
        theta(i,j) = y(i)
     enddo
  enddo
END SUBROUTINE gridgeneration

END MODULE gridgen
