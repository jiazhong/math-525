MODULE functions

CONTAINS

   FUNCTION TRID(Sub,Diag,Sup,B,N)
    implicit none
    integer           :: k, tmp, N
    double precision  :: ratio
    double precision  :: Sub(N), Diag(N), Sup(N-1), B(N), TRID(N)

    do k = 2,N
       ratio = -Sub(k)/Diag(k-1)
       Diag(k) = Diag(k) + ratio*Sup(k-1)
       B(k) = B(k) + ratio*B(k-1)
    enddo

    B(N) = B(N)/Diag(N)
    k = N

    do tmp = 2,N
       k = k-1
       B(k) = (B(k)-Sup(k)*B(k+1))/Diag(k)
    enddo

    TRID = B
  END FUNCTION TRID

  FUNCTION Vr(j,i,dx_fld,dy)
    implicit none
    integer           :: j, i
    double precision  :: dx_fld, dy
    double precision  :: tmp1, tmp2, tmp3, Vr

    tmp1 = 2*i**2*dx_fld**2
    tmp2 = -3*i*dx_fld
    tmp3 = 1/(i*dx_fld)
    Vr   = -0.5*(tmp1+tmp2+tmp3)*cos(j*dy)
  END FUNCTION Vr

  FUNCTION Vo(j,i,dx_fld,dy)
    implicit none
    integer           :: j, i
    double precision  :: dx_fld, dy
    double precision  :: tmp1, tmp2, tmp3, Vo

    tmp1 = 4*i*dx_fld
    tmp2 = -3/(i*dx_fld)
    tmp3 = -1/(i**3*dx_fld**3)
    Vo   = 0.25*(tmp1+tmp2+tmp3)*sin(j*dy)
  END FUNCTION Vo

END MODULE functions
