MODULE solver
CONTAINS
SUBROUTINE load_xmatrix1
  use globaldef
  use functions
  call para

  Sub_1(1) = 0
  do i = 2,prt-1
     Sub_1(i) = F1*(1/(i-1)-1);
  enddo
  Sub_1(prt) = 1
  do i = prt+1,final-1
     Sub_1(i) = F2*(1/(i-1)-1) - Fc*Vr(0,i-1,dx_fld,dy);
  enddo
  Sub_1(final) = 0

  Diag_1(1) = 1+6*F1
  do i = 2,prt-1
     Diag_1(i) = 1+2*F1
  enddo
  Diag_1(prt) = -(1+(k2/k1)*(dx_spher/dx_fld))
  do i = prt+1,final-1
     Diag_1(i) = 1+2*F2
  enddo
  Diag_1(final) = 1;

  Sup_1(1) = -6*F1
  do i = 2,prt-1
     Sup_1(i) = -F1*(1/(i-1)+1)
  enddo
  Sup_1(prt) = (k2/k1)*(dx_spher/dx_fld)
  do i = prt+1,final-1
     Sup_1(i) = -F2*(1/(i-1)+1) + Fc*Vr(0,i-1,dx_fld,dy)
  enddo

  B_1(1) = Z(1,1,k-1)
  do i = 2,prt-1
     B_1(i) = (1-2*F1/((i-1)**2*dy**2))*Z(1,i,k-1) &
            & + (2*F1/((i-1)**2*dy**2))*Z(2,i,k-1)
  enddo
  B_1(prt) = 0
  do i = prt+1,final-1
     B_1(i) = (1-2*F2/((i-1)**2*dy**2))*Z(1,i,k-1) &
            & + (2*F2/((i-1)**2*dy**2))*Z(2,i,k-1)
  enddo
  B_1(final) = 0
END SUBROUTINE load_xmatrix1

SUBROUTINE load_xmatrix2
  use globaldef
  use functions
  call para

  Sub_1(1) = 0
  do i = 2,prt-1
     Sub_1(i) = F1*(1/(i-1)-1)
  enddo
  Sub_1(prt) = 1
  do i = prt+1,final-1
     Sub_1(i) = F2*(1/(i-1)-1) - Fc*Vr(j-1,i-1,dx_fld,dy)
  enddo
  Sub_1(final) = 0

  Diag_1(1) = 1+6*F1
  do i = 2,prt-1
     Diag_1(i) = 1+2*F1
  enddo
  Diag_1(prt) = -(1+(k2/k1)*(dx_spher/dx_fld))
  do i = prt+1,final-1
     Diag_1(i) = 1+2*F2
  enddo
  Diag_1(final) = 1

  Sup_1(1) = -6*F1
  do i = 2,prt-1
     Sup_1(i) = -F1*(1/(i-1)+1)
  enddo
  Sup_1(prt) = (k2/k1)*(dx_spher/dx_fld)
  do i = prt+1,final-1
     Sup_1(i) = -F2*(1/(i-1)+1) + Fc*Vr(j-1,i-1,dx_fld,dy)
  enddo

  B_1(1) = Z(j,1,k-1)
  do i = 2,prt-1
     B_1(i) = (F1/(i-1)**2)*(1/dy**2 - 1/(tan((j-1)*dy)*2*dy))*Z(j-1,i,k-1) &
           & + (1-2*F1/((i-1)**2*dy**2))*Z(j,i,k-1) &
           & + (F1/(i-1)**2)*(1/dy**2 + 1/(tan((j-1)*dy)*2*dy))*Z(j+1,i,k-1)
  enddo
  B_1(prt) = 0
  do i = prt+1,final-1
     B_1(i) = ((F2/(i-1)**2)*(1/dy**2 - 1/(tan((j-1)*dy)*2*dy)) + (Fc/(i-1))*(Vo(j-1,i-1,dx_fld,dy)/dy))*Z(j-1,i,k-1) &
      & + (1-2*F2/((i-1)**2*dy**2))*Z(j,i,k-1) &
      & + ((F2/(i-1)**2)*(1/dy**2 + 1/(tan((j-1)*dy)*2*dy)) - (Fc/(i-1))*(Vo(j-1,i-1,dx_fld,dy)/dy))*Z(j+1,i,k-1)
  enddo
  B_1(final) = 0
END SUBROUTINE load_xmatrix2

SUBROUTINE load_xmatrix3
  use globaldef
  use functions
  call para

  Sub_1(1) = 0
  do i = 2,prt-1
     Sub_1(i) = F1*(1/(i-1)-1)
  enddo
     Sub_1(prt) = 1
  do i = prt+1,final-1
     Sub_1(i) = F2*(1/(i-1)-1) - Fc*Vr(n_y-1,i-1,dx_fld,dy)
  enddo
  Sub_1(final) = 0

  Diag_1(1) = 1+6*F1
  do i = 2,prt-1
     Diag_1(i) = 1+2*F1
  enddo
  Diag_1(prt) = -(1+(k2/k1)*(dx_spher/dx_fld))
  do i = prt+1,final-1
     Diag_1(i) = 1+2*F2
  enddo
  Diag_1(final) = 1

  Sup_1(1) = -6*F1
  do i = 2,prt-1
     Sup_1(i) = -F1*(1/(i-1)+1)
  enddo
  Sup_1(prt) = (k2/k1)*(dx_spher/dx_fld)
  do i = prt+1,final-1
     Sup_1(i) = -F2*(1/(i-1)+1) + Fc*Vr(n_y-1,i-1,dx_fld,dy)
  enddo

  B_1(1) = Z(n_y,1,k-1)
  do i = 2,prt-1
     B_1(i) = (2*F1/((i-1)**2*dy**2))*Z(n_y-1,i,k-1) &
          & + (1-2*F1/((i-1)**2*dy**2))*Z(n_y,i,k-1)
  enddo
  B_1(prt) = 0
  do i = prt+1,final-1
     B_1(i) = (2*F2/((i-1)**2*dy**2))*Z(n_y-1,i,k-1) &
          & + (1-2*F2/((i-1)**2*dy**2))*Z(n_y,i,k-1)
  enddo
  B_1(final) = 0
END SUBROUTINE load_xmatrix3

SUBROUTINE load_ymatrix1
  use globaldef
  use functions
  call para

  Sub_2(1) = 0 
  do j = 2,n_y-1
     tmp1 = F1/(i-1)**2
     tmp2 = 1/(tan((j-1)*dy)*2*dy)
     tmp3 = 1/(dy**2)
     Sub_2(j) = tmp1*(tmp2-tmp3)
  enddo
  Sub_2(n_y) = -2*F1/((i-1)**2*dy**2)

  Diag_2(1) = 1+2*F1/((i-1)**2*dy**2)
  do j = 2,n_y-1
     Diag_2(j) = 1+2*F1/((i-1)**2*dy**2)
  enddo
  Diag_2(n_y) = 1+2*F1/((i-1)**2*dy**2)

  Sup_2(1) = -2*F1/((i-1)**2*dy**2)
  do j = 2,n_y-1
     tmp1 = F1/(i-1)**2
     tmp2 = 1/(tan((j-1)*dy)*2*dy)
     tmp3 = 1/(dy**2)
     Sup_2(j) = -tmp1*(tmp2+tmp3)
  enddo

  B_2(1) = F1*(1-1/(i-1))*Z(1,i-1,k-1) &
           & + (1-2*F1)*Z(1,i,k-1) &
           & + F1*(1+1/(i-1))*Z(1,i+1,k-1)
  do j = 2,n_y-1
     B_2(j) = F1*(1-1/(i-1))*Z(j,i-1,k-1) &
              & + (1-2*F1)*Z(j,i,k-1) &
              & + F1*(1+1/(i-1))*Z(j,i+1,k-1)
  enddo
  B_2(n_y) = F1*(1-1/(i-1))*Z(n_y,i-1,k-1) &
             & + (1-2*F1)*Z(n_y,i,k-1) &
             & + F1*(1+1/(i-1))*Z(n_y,i+1,k-1)
END SUBROUTINE load_ymatrix1

SUBROUTINE load_ymatrix2
  use globaldef
  use functions
  call para

  Sub_2(1) = 0
  do j = 2,n_y-1
     tmp1 = F2/(i-1)**2
     tmp2 = 1/(tan((j-1)*dy)*2*dy)
     tmp3 = 1/(dy**2)
     Sub_2(j) = tmp1*(tmp2-tmp3) - (Fc/(i-1))*(Vo(j-1,i-1,dx_fld,dy)/dy)
  enddo
  Sub_2(n_y) = -2*F2/((i-1)**2*dy**2)

  Diag_2(1) = 1+2*F2/((i-1)**2*dy**2)
  do j = 2,n_y-1
     Diag_2(j) = 1+2*F2/((i-1)**2*dy**2)
  enddo
  Diag_2(n_y) = 1+2*F2/((i-1)**2*dy**2)

  Sup_2(1) = -2*F2/((i-1)**2*dy**2)
  do j = 2,n_y-1
     tmp1 = F2/(i-1)**2
     tmp2 = 1/(tan((j-1)*dy)*2*dy)
     tmp3 = 1/(dy**2)
     Sup_2(j) = -tmp1*(tmp2+tmp3) + (Fc/(i-1))*(Vo(j-1,i-1,dx_fld,dy)/dy)
  enddo

  B_2(1) = (F2*(1-1/(i-1)) + Fc*Vr(0,i-1,dx_fld,dy))*Z(1,i-1,k-1) &
           & + (1-2*F2)*Z(1,i,k-1) &
           & + (F2*(1+1/(i-1)) - Fc*Vr(0,i-1,dx_fld,dy))*Z(1,i+1,k-1)
  do j = 2,n_y-1
         B_2(j) = (F2*(1-1/(i-1)) + Fc*Vr(j-1,i-1,dx_fld,dy))*Z(j,i-1,k-1) &
         & + (1-2*F2)*Z(j,i,k-1) &
         & + (F2*(1+1/(i-1)) - Fc*Vr(j-1,i-1,dx_fld,dy))*Z(j,i+1,k-1)
  enddo
  B_2(n_y) = (F2*(1-1/(i-1)) + Fc*Vr(n_y-1,i-1,dx_fld,dy))*Z(n_y,i-1,k-1) &
             & + (1-2*F2)*Z(n_y,i,k-1) &
             & + (F2*(1+1/(i-1)) - Fc*Vr(n_y-1,i-1,dx_fld,dy))*Z(n_y,i+1,k-1)
END SUBROUTINE load_ymatrix2

END MODULE solver
