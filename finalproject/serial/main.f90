PROGRAM main

  use mpi
  use functions 
  use globaldef
  use gridgen
  use timegen
  use paragen
  use solver

  implicit none

  pi = 4d0*atan(1d0)

  call mpi_init(ierror)
  call mpi_comm_size(comm, p, ierror)
  call mpi_comm_rank(comm, rank, ierror)
  call gridgeneration
  call timegeneration
  call parageneration

  allocate(Z(n_y, final, n_t_adi))

  allocate(Sub_1(final))
  allocate(Diag_1(final))
  allocate(Sup_1(final-1))
  allocate(B_1(final))

  allocate(Sub_2(n_y))
  allocate(Diag_2(n_y))
  allocate(Sup_2(n_y-1))
  allocate(B_2(n_y))

  do j = 1,n_y
     do i = 1,prt
        Z(j,i,1) = 1
     enddo
  enddo

  t0 = mpi_wtime()

  do k = 2,n_t_adi
     if (mod(k,2)==0) then
        call load_xmatrix1
        Z(1,:,k) = TRID(Sub_1,Diag_1,Sup_1,B_1,final)
        do j = 2,n_y-1
           call load_xmatrix2
           Z(j,:,k) = TRID(Sub_1,Diag_1,Sup_1,B_1,final)
        enddo
        call load_xmatrix3
        Z(n_y,:,k) = TRID(Sub_1,Diag_1,Sup_1,B_1,final)
     else
        Z(:,1,k) = Z(:,1,k-1)
        do i = 2,prt-1
           call load_ymatrix1
           Z(:,i,k) = TRID(Sub_2,Diag_2,Sup_2,B_2,n_y)
        enddo
        Z(:,prt,k) = Z(:,prt,k-1)
        do i = prt+1,final-1
           call load_ymatrix2
           Z(:,i,k) = TRID(Sub_2,Diag_2,Sup_2,B_2,n_y)
        enddo
        Z(:,final,k) = Z(:,final,k-1)
     endif
  enddo     

  t1 = mpi_wtime()

  print*,'time=', t1-t0

  !open(unit=10,file='output.dat')
  !write(10,*) 'n_x', final, 'n_y', n_y, 'n_t', n_t
  !do k = 1, n_t
  !   do i = 1, n_y
  !      do j = 1, final
  !         write(10, '(E16.8\)') R(i,j)
  !         write(10, '(E16.8\)') theta(i,j)
  !         write(10, '(E16.8)') Z(i,j,2*(k-1)+1)
  !      enddo
  !   enddo
  !enddo
  call mpi_finalize(ierror)
END PROGRAM main
